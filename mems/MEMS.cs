﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mems
{
    public partial class MEMS : Form
    {
        public MEMS()
        {
            InitializeComponent();
        }

        private void Plane_Click(object sender, EventArgs e)
        {
            var formPlane = new FormPlane();
            formPlane.Show();
        }

        private void Mixer_Click(object sender, EventArgs e)
        {
            var formMixer = new FormMixer();
            formMixer.Show();
        }
    }
}
