﻿namespace mems
{
    partial class MEMS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Plane = new System.Windows.Forms.Button();
            this.Mixer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Plane
            // 
            this.Plane.Location = new System.Drawing.Point(150, 50);
            this.Plane.Name = "Plane";
            this.Plane.Size = new System.Drawing.Size(200, 100);
            this.Plane.TabIndex = 0;
            this.Plane.Text = "Open Plane Configuration";
            this.Plane.UseVisualStyleBackColor = true;
            this.Plane.Click += new System.EventHandler(this.Plane_Click);
            // 
            // Mixer
            // 
            this.Mixer.Location = new System.Drawing.Point(150, 200);
            this.Mixer.Name = "Mixer";
            this.Mixer.Size = new System.Drawing.Size(200, 100);
            this.Mixer.TabIndex = 1;
            this.Mixer.Text = "Open Mixer Configuration";
            this.Mixer.UseVisualStyleBackColor = true;
            this.Mixer.Click += new System.EventHandler(this.Mixer_Click);
            // 
            // MEMS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 353);
            this.Controls.Add(this.Mixer);
            this.Controls.Add(this.Plane);
            this.Name = "MEMS";
            this.Text = "Voloshyn | ";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Plane;
        private System.Windows.Forms.Button Mixer;
    }
}

